# Dicoding x Dottom

![tampilan-antarmuka](assets/img/web-tester.png)

Info Submission klik [disini](https://medium.com/@tommyalhamra/raja-terkahir-dicoding-bfc9ca5f544e)

Thanks **IDCamp** [Beasiswa - Pelatihannya](https://idcamp.indosatooredoo.com/)

Indosat Ooredoo Digital Camp (IDCamp) adalah sebuah program beasiswa dari Indosat Ooredoo untuk mencetak developer/programmer muda Indonesia yang siap bersaing di dunia ekonomi digital.

---
### Syaratnya
- [x] Semantic HTML 5
- [x] Tampilkan Identitas di dalam element aside
- [x] Susun layout menggunakan Float atau Flexbox
- [x] Responsive Web
- [x] Menerapkan Javascript dalam memanipulasi DOM
- [x] No Framework

Terimakasih Dicoding!

> Susahnya Belajar Fundamental